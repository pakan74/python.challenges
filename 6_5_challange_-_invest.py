def invest(amount, rate, years):
    for year in range(years):
        amount = amount + (amount * rate)
        print(f"year {year + 1}: ${amount:,.2f}")
    return

amount = int(input("Enter initial amount: "))
rate = float(input("Enter annual percentage rate: "))
years = int(input("Enter a number of years: "))

invest(amount, rate, years)
