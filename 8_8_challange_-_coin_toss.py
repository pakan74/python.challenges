"""Usunięcie # umożliwia sprawdzenie przy małęj ilości powtórzeń np 20"""

from random import randint

def flip():
    return randint(0,1)

trials = int(input("Enter a number of trials: "))
flip_number = 0
sequence_number = 0
total_flip_number = 0
coin = flip()

#print(coin)

for i in range(1, trials):
    result = flip()
    #print(result)
    total_flip_number = total_flip_number + flip_number
    if coin == result:
        flip_number = flip_number + 1
    else:
        sequence_number = sequence_number + 1
        flip_number = 1
    coin = result

    #print(f"coin - {coin}")
    #print(f" seq = {sequence_number}")
    #print(f" flip = {flip_number}")

avarage = total_flip_number/sequence_number

print(f"Average flips per trial: {avarage:.1f}")
print(f"Total flip in sequences: {total_flip_number}")
print(f"Number of sequences: {sequence_number}")
