def convert_cel_to_far(temp):
    """Convert temperature from Celsius to Fahrenheit"""
    temp_f = temp * 9/5 + 32
    return temp_f

def convert_far_to_cel(temp):
    """Confert temperature from Fahrenheit to Celsius"""
    temp_c = (temp - 32) * 5/9
    return temp_c

user_temp_f = float(input("Enter a temperature in degrees F: "))
print(f"{user_temp_f} degrees F = {convert_far_to_cel(user_temp_f):.2f} degrees C")

user_temp_c = float(input("Enter a temperature in degrees C: "))
print(f"{user_temp_c} degrees C = {convert_cel_to_far(user_temp_c):.2f} degrees F")

    
