user_integer = int(input("Enter a positive integer: "))

for factor in range(1, user_integer + 1):
    if user_integer % factor == 0:
        print(f"{factor} is a factor of {user_integer}")
